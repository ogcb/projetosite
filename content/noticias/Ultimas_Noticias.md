---
title: "Últimas Notícias"
date: 2021-10-22T23:33:28-03:00
draft: false
---

## AUXILIARES TÉCNICOS DA SELEÇÃO BRASILEIRA MARCAM PRESENÇA NAS SEMIFINAIS DA COPA DO BRASIL

#### 18/10/2021 às 12:48 

No intervalo entre uma Data FIFA e outra, os auxiliares técnicos da Seleção Brasileira seguem o trabalho de observações 'in loco' de atletas que atuam no Brasil. Nesta quarta-feira (20), quatro deles irão se dividir para acompanhar as semifinais da Copa do Brasil. Cléber Xavier e César Sampaio estarão em Belo Horizonte para acompanhar Atlético-MG e Fortaleza, enquanto Matheus Bachi e Bruno Baquete vão a Curitiba para assistir ao duelo entre Athletico e Flamengo.

Atlético-MG e Fortaleza medem forças a partir das 21h30 desta quarta-feira (20). A partida será realizada no Estádio Mineirão. Enquanto o Galo chega para esta semifinal após eliminar o Fluminense na fase anterior, o tricolor cearense vem de vitória em cima do São Paulo na quartas de final. O jogo de volta entre as duas equipes será na próxima semana, no dia 27 de outubro. 

Do outro lado da chave, Athletico e Flamengo iniciam o confronto desta quarta-feira (20) também às 21h30. O jogo de ida, desta noite, será na Arena da Baixada, enquanto a volta está marcada para o dia 27 de outubro, no Maracanã. Na fase anterior, as duas equipes eliminaram seus adversários com duas vitórias. O Furacão passou pelo Santos e o Rubro-Negro carioca derrotou o Grêmio.

A Seleção Brasileira, por sua vez, volta a entrar em campo no dia 11 de novembro, em partida válida pelas Eliminatórias da Copa do Mundo de 2022. O duelo será com a Colômbia, na NeoQuímica Arena. Com 31 pontos conquistados até aqui, o Brasil é o líder isolado da classificação sul-americana para o Mundial, com seis pontos à frente do segundo colocado, a Argentina. 

![erro]({{< resource url="/images/tecni.png" >}})

---

##  TITE REVERENCIA TORCIDA APÓS GOLEADA DA SELEÇÃO

#### 15/10/2021 às 03:28 

Após a goleada por 4 a 1 sobre o Uruguai, o técnico da Seleção Brasileira, Tite, agradeceu os torcedores pelo apoio na Arena da Amazônia, em Manaus (AM).

Em entrevista coletiva após a partida, o treinador ressaltou o carinho demonstrado pelos torcedores, não só na partida desta quinta-feira (14), como nos últimos dias:

“Obrigado, Manaus. Tiveram calor humano extraordinário. No retorno do público, com crianças nos treinamentos. Quando chegava o ônibus. Calor humano é exemplar. Foi muito bonito”, disse, antes de dividir o mérito da goleada com os próprios torcedores:

”Talvez a atuação de hoje tenha sido um pouco o retorno de todo esse carinho, essa receptividade”.

Dentro de campo, Tite viu uma Seleção que evoluiu, e que segue um processo constante de desenvolvimento de um padrão de jogo.

“Sempre falo que são estágios, processos. Jogo acaba mostrando, o tempo vai permitindo. Experiência que tenho passado, nem euforia em demasia. Foi grande jogo, teve solidez. Futebol é relação de conjunto, peso, contrapeso. Foi grande jogo sim”, concluiu.

Com a vitória por 4 a 1 sobre o Uruguai, a Seleção Brasileira chegou a 31 pontos e segue líder isolada das Eliminatórias da Copa do Mundo FIFA Catar 2022. Os gols do Brasil foram marcados por Neymar, Raphinha (duas vezes) e Gabi. Luís Suárez descontou para o Uruguai.

![erro]({{< resource url="/images/tite.png" >}})

---

##  ARTILHEIRO DO BRASIL NAS ELIMINATORIAS, NEYMAR MARCA 70º GOL PELA SELEÇÃO

#### 13/10/2021 às 02:51 

Foi um jogo e tanto para Neymar Jr. No primeiro contato com a torcida brasileira, o atacante marcou um gol, deu duas assistências e foi ovacionado pelos torcedores presentes na goleada do Brasil por 4 a 1 sobre o Uruguai, na Arena da Amazônia.

Desde a saída do time para o aquecimento, Neymar já teve uma amostra do que seria sua noite. Muito calor e receptividade da torcida manauara, que entoava seu nome a cada aceno e toque na bola. Dentro de campo, bastou a bola rolar para que o camisa 10 retribuísse o carinho com o que mais sabe fazer: gol.

Foi dele o primeiro do Brasil na goleada contra o Uruguai. Após grande passe de Fred, Neymar dominou dentro da área, limpou o goleiro Muslera e finalizou com categoria para abrir o placar. Este foi o 70º gol de Neymar com a camisa da Seleção Brasileira e o sétimo dele nas Eliminatórias. O atacante é o artilheiro da Seleção no torneio e o segundo maior goleador da competição.

Depois, o chute dele parou no goleiro uruguaio, mas Raphinha estava esperto para completar para a rede. Na segunda etapa, Neymar Jr. seguiu dando show. Em ataque de alta velocidade, ele deixou Raphinha na boa para fazer o terceiro. E, quando o Brasil vencia por 3 a 1, foi dele o cruzamento precioso para Gabi completar o placar: 4 a 1. Uma atuação de luxo para os mais de 12 mil presentes na Arena da Amazônia, que cantaram em uníssono: "olê, olê, olê, olá, Neymar, Neymar!".

Com a vitória, o Brasil segue na liderança isolada das Eliminatórias da Copa do Mundo FIFA, com 31 pontos em 11 partidas (dez vitórias e um empate).

![erro]({{< resource url="/images/neymar.png" >}})
