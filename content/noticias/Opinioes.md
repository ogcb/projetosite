---
title: "Opiniões dos Torcedores"
date: 2021-10-22T23:34:28-03:00
draft: false
---

## FALTOU O BARBOSA DE 2014 PARA O BRASIL NÃO PERDER DE 7X1 PARA A ALEMANHA

##### POR OTAVIO BITTENCOURT

Quem é o vilão do 7 a 1?

Em 1950, na primeira derrota de Copa em casa, para o Uruguai, Barbosa, o goleiro, foi o escolhido. Uma das maiores injustiças da historia do futebol.

Barbosa falhou em ao menos um dos gols no 2 a 1 do Maracanazo, como ficou conhecida a derrota.

Pouco para crucificar um jogador, levando em conta que o time tinha craques do calibre de Zizinho, Ademir Menezes e Jair Rosa Pinto.  Que não resolveram.

Em 2014, se fala do 7 a 1 já como algo mítico, como se tivesse acontecido sem personagens.

Lembra-se de falhas defensivas, se fala que a geração é fraca, que Felipão escalou mal, mas ele esteve longe de ser crucificado como Barbosa.

Mas revendo o jogo, percebe-se o motivo de não ter vilões (esqueça também o colombiano Zuñiga, que tirou Neymar da Copa nas quartas de final).

E uma frase relembrada pelo colunista Paulo Vinícius Coelho em seu texto publicado na Folha, nesta quarta (8), é cirúrgica para diagnosticar o fato: disse o ex-zagueiro Márcio Santos, campeão do mundo em 1994, depois de o Brasil sofrer, e chorar, para vencer o Chile nos pênaltis nas oitavas de final.

“Está todo mundo com medo de ser o Barbosa”, disse Márcio Santos, segundo PVC.

Não houve um Barbosa. Porque se a falha tivesse sido de apenas um atleta, o jogo teria sido 2 a 1, não 7 a 1. Ninguém assumiu a responsabilidade, e quem tentou não tinha competência para isso.

Com menos de cinco minutos de jogo, David Luiz, o capitão naquela tarde, que é zagueiro, estava na ponta direita tentando uma jogada. Cinco minutos, um zagueiro na ponta direita.

O escanteio que originou o primeiro gol, saiu de um passe errado de Marcelo na ponta esquerda.

Oscar, que era o armador, não tocou na bola no primeiro tempo e Fernandinho, o volante, precisou tomar a responsabilidade. Foi ele que errou o passe do quarto gol quando tentava sair jogando, que fez Galvão Bueno soltar uma das frases famosas do 7 a 1, a “virou passeio”.

É preciso renovar o futebol brasileiro. Melhorar a formação dos jogadores, tentar evitar o êxodo de jovens, modernizar nossos treinadores, rejuvenescer os cartolas que estão no poder há décadas.

Perder três Copas seguidas, ser eliminado nas quartas de final em duas Copas Américas, e ver hoje o time no mesmo nível de rivais como Chile e Colômbia, é reflexo de tudo o descrito acima.

O 7 a 1, não. O 7 a 1 foi um conjunto de falhas de vários jogadores que nunca quiseram ser o Barbosa da vez.


![erro]({{< resource url="/images/triste.png" >}})

---


## O MEDO DE PELA PRIMEIRA VEZ FICAR FORA DE UMA COPA DO MUNDO

##### POR LUCAS VASQUES

De Concepción – A segunda pergunta que todos os jogadores da seleção mais ouviram após a derrota nos pênaltis para o Paraguai, no sábado à noite, foi se o Brasil corre o risco de pela primeira vez ficar fora de uma Copa do Mundo (a primeira foi se a virose realmente atrapalhou a atuação do time).

Desde julho de 2014, quando o Brasil foi massacrado por Alemanha e Holanda na reta final da Copa, e como postado no blog em janeiro, a CBF e a nova comissão técnica da seleção liderada por Gilmar Rinaldi e Dunga avaliam que a próxima eliminatória, para a Copa-2018, na Rússia, será a mais difícil da história.

Motivos não faltavam (e não faltam): a boa campanha dos sul-americanos na Copa-2014, na época a possibilidade da perda de uma vaga no Mundial para a América do Sul (que não foi confirmada, se mantém quatro diretas e uma via repescagem) e, claro, o fraco desempenho do Brasil em campo.

Dunga foi contratado por “entender o espírito da seleção brasileira, para devolver aos jogadores a vontade de defender essa camisa”. Foram mais ou menos essas palavras que o então presidente da CBF José Maria Marin usou para explicar a opção pelo técnico. O recado era: nas eliminatórias, vale mais a garra do que a técnica ou tática.

A Copa América mostrou que não é bem assim. Na tática e na técnica, principalmente quando Neymar está fora, o Brasil se equipara a times médios do continente, como Paraguai e até Peru e Venezuela. Contra a Colômbia, o Brasil foi dominado na derrota por 1 a 0, e Neymar estava em campo.

Mas mesmo a garra que poderia fazer a diferença, acabou se transformando em nervosismo. Exemplos são Neymar xingando o árbitro na porta do vestiário e jogadores batendo boca com adversários após provocações que são costumeiras na América do Sul.

No classificatório para a Copa, com nove jogos como visitante, em estádios muitas vezes acanhados, com altitude, a pressão e provocação serão ainda maiores.

Para piorar, a CBF politicamente está enfraquecida na Conmebol, depois da volta de Marco Polo Del Nero ao Brasil, em maio, antes da eleição para presidente da Fifa, em meio às prisões de cartolas da entidade (entre eles seu aliado Marin). O Brasil também não seguiu a indicação de voto em bloco da confederação sul-americana na oposição a Joseph Blatter para presidir a entidade mundial.

Para piorar ainda mais um pouco, Neymar está suspenso para as duas primeiras partidas da eliminatória, em outubro, ainda cumprindo a pena por ter xingado o árbitro chileno Enrique Osses.

Mesmo assim, com todo esse cenário negativo, nenhum jogador admitiu que o Brasil pode pela primeira vez ficar fora de uma Copa. Otimismo ainda é palavra de ordem no time de Dunga.]

![erro]({{< resource url="/images/colo.png" >}})
